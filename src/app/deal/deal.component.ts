import { Component, OnInit } from '@angular/core';
import { DealsService } from '../services/Deals.service';
import {Deal} from '../Deal';

@Component({
  selector: 'app-deal',
  templateUrl: './deal.component.html',
  styleUrls: ['./deal.component.css'],
  providers :[DealsService]

})
export class DealComponent  {
  recommanded_deals;
  _deals:Deal[];
  constructor(private deals:DealsService){
    this._deals=[];
    this.recommanded_deals=[];
      //this._deals= this.deals.getListDeals();
    this.deals.getRecommandedDeals().subscribe(data => {this.recommanded_deals = data});
    this.deals.getListDeals().subscribe(data => {this._deals = data});
 }

}
