import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaildealComponent } from './detaildeal.component';

describe('DetaildealComponent', () => {
  let component: DetaildealComponent;
  let fixture: ComponentFixture<DetaildealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetaildealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaildealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
