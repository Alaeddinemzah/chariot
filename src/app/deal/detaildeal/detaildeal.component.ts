import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { DealsService } from '../../services/Deals.service';
import {Deal} from '../../Deal';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-detaildeal',
  templateUrl: './detaildeal.component.html',
  styleUrls: ['./detaildeal.component.css'],
  providers :[DealsService]
 
})
export class DetaildealComponent implements OnInit {


  constructor( private route: ActivatedRoute,service :DealsService) { 
      this.p=[];
   // this.deal= service.getDeal(1);
  this.deal ={id: 1,
            title: "Smartphone HUAWEI GR5 2017 4G",
            description :  "Ecran 5.5\ FHD \n\n- Résolution: 1080 x 1920 pixels \n\n- Processeur: Kirin 655,  Octa-Core ( 4x2.1 Ghz + 4x1.7Ghz ) \n\n- Systéme d'exploitation: AndroidTM 6.0+EMUI 4.1 \n\n- Mémoire RAM: 3 Go  \n\n- Stockage:  32 Go de mémoire Extensible Jusqu'à 128 Go \n\n- Double SIM \n\n-  Appareil Photo Arriére (double camera) 12 MP +2 MP camera\n\n- Frontale 8.0 Mégapixel \n\n- Connctivité: 4G - Wifi et Bluetooth \n\n- Garantie 1 an",
            price: 799,
            nbPurshaseR: 2,
            nbPurshase: 2,
            releaseDate: "2018-11-16 00:00:00",
            expiredDate: "2017-11-16 00:00:00",
            status: "Encours",
            category_id: 4,
            created_at: "2020-11-13 22:29:21",
            created_at_human: "2 years from now",
            user: {
                data: {
                    id: 3,
                    firstName: "Alaeddine",
                    lastName: "GH1",
                    adress: "Ariana essoughra",
                   
                    username: "FG1_client",
                    email: "client@esprit.tn",
                    tel1: "+2165848675",
                    tel2: "+2165454825",
                    deliveryAdress: "00000000",
                    role:"client",
                    image: {
                        data: {
                            id: 19,
                            imgLink: "assets/img/users/atomix_user31.png",
                            objectType: "user",
                            created_at: "2017-11-28 15:40:19",
                            created_at_human: "5 days ago"
                        }
                    }
                }
            }
        }
  }
  
deal;
p;
addToPanier()
 {
  this.p = JSON.parse(localStorage.getItem("panier"));
  if(this.p==null)
  {
      this.p=[];
  }
   var panier={id:this.deal.id,"title":this.deal.title,"price":this.deal.price,"qte":1};
   this.p.push(panier);
   localStorage.setItem("panier", JSON.stringify(this.p));
 }
 ngOnInit() {
   // this.deal=this.route.params.switchMap(params => this.service.getDeal(params['id']));
 

   
  }

}
