import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../services/Products.service';
import {Product} from '../Product';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers :[ProductsService]
})
export class ProductComponent implements OnInit {

  list_products;//:Product[];
  list_recommanded;
  constructor(private deals:ProductsService){
    this.list_recommanded=[];
    this.deals.getRecommandedProducts().subscribe(data => {this.list_recommanded = data});
    this.list_products=[];
    this.deals.getListProducts().subscribe(data => {this.list_products = data});
  }

  ngOnInit() {
  }

}
