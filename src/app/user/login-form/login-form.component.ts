import { Component, OnInit } from '@angular/core';
import {AppService} from '../../services/app.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
    /*    
      if(user.email=='admin' && user.password=='admin')
      {
         console.log(user.email+'kjk'+user.password); 
        localStorage.setItem('currentUser', 'admin');
      }*/
   
  loginInput = 'client1@esprit.tn';
  passwordInput = '0000';

  constructor(private appService: AppService) {
  }

  ngOnInit() {
  }

  login() {
    this.appService.login(this.loginInput, this.passwordInput);
  }

}
