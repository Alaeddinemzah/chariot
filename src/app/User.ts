import{Image} from './Image';
export class User {
id : number;
firstName :string;
lastName:string;
tel1:string;
tel2:string;
username:string;
email:string;
password? : string;
adress  :string;
images?:Image[];
image?:Image;
image_id:number;
deliveryAdress :string;
role :string;
 created_at:string;
   updated_at:string;
}