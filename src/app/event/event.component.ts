import { Component, OnInit } from '@angular/core';
import { EventsService } from '../services/Events.service';
import {Event} from '../Event';
@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css'],
  providers :[EventsService]
})
export class EventComponent implements OnInit {

  list_events;//:Deal[];

  constructor(private event_service:EventsService){
    this.list_events=[];
    this.event_service.getListEvents().subscribe(data => {this.list_events = data});
  }

  ngOnInit() {
  }

}
