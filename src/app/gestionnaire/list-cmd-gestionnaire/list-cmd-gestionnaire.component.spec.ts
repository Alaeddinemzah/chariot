import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCmdGestionnaireComponent } from './list-cmd-gestionnaire.component';

describe('ListCmdGestionnaireComponent', () => {
  let component: ListCmdGestionnaireComponent;
  let fixture: ComponentFixture<ListCmdGestionnaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCmdGestionnaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCmdGestionnaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
