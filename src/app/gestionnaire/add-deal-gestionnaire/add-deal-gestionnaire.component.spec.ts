import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDealGestionnaireComponent } from './add-deal-gestionnaire.component';

describe('AddDealGestionnaireComponent', () => {
  let component: AddDealGestionnaireComponent;
  let fixture: ComponentFixture<AddDealGestionnaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDealGestionnaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDealGestionnaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
