import { Component, OnInit } from '@angular/core';
import {User} from '../../User';
//import {DataJson} from '../../Data_json';
import {UsersService} from '../../services/Users.service';
@Component({
  selector: 'app-list-clients',
  templateUrl: './list-clients.component.html',
  styleUrls: ['./list-clients.component.css'],
  providers :[UsersService]
})
export class ListClientsComponent implements OnInit {

  ngOnInit() {
  }
 list_users:User[];
 //data :DataJson;
   constructor(private user_service:UsersService) {
   this.user_service.getListUsers().subscribe(data => {this.list_users = data});

 console.log(this.list_users);
  }
    getUserById(iduser:number) 
  {
    for(let result of this.list_users){

        if(result.id==iduser){
          return result;
        }
    }
  }

}
