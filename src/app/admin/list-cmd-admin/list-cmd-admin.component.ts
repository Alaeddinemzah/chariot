import { Component, OnInit } from '@angular/core';
import {Command} from '../../Command';
//import {DataJson} from '../../Data_json';
import {CommandsService} from '../../services/Commands.service';
@Component({
  selector: 'app-list-cmd-admin',
  templateUrl: './list-cmd-admin.component.html',
  styleUrls: ['./list-cmd-admin.component.css'],
  providers :[CommandsService]
})
export class ListCmdAdminComponent implements OnInit {

  list_commands:Command[];
  constructor(private command_service:CommandsService) {
    this.command_service.getListCommands().subscribe(data => {this.list_commands = data});
    console.log(this.list_commands);
  }

  ngOnInit() {
  }

}
