import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCmdAdminComponent } from './list-cmd-admin.component';

describe('ListCmdAdminComponent', () => {
  let component: ListCmdAdminComponent;
  let fixture: ComponentFixture<ListCmdAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCmdAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCmdAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
