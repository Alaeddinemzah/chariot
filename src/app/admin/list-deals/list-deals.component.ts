import { Component, OnInit } from '@angular/core';
import {Deal} from '../../Deal';
//import {DataJson} from '../../Data_json';
import {DealsService} from '../../services/Deals.service';
@Component({
  selector: 'app-list-deals',
  templateUrl: './list-deals.component.html',
  styleUrls: ['./list-deals.component.css'],
   providers :[DealsService],
})
export class ListDealsComponent implements OnInit {

 ngOnInit() {
  }
 list_deals:Deal[];
 //data :DataJson;
   constructor(private deal_service:DealsService) {
   this.deal_service.getListDeals().subscribe(data => {this.list_deals = data});


   }

}
