import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDealAdminComponent } from './add-deal-admin.component';

describe('AddDealAdminComponent', () => {
  let component: AddDealAdminComponent;
  let fixture: ComponentFixture<AddDealAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDealAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDealAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
