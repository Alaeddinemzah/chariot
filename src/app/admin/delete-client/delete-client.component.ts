import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delete-client',
  templateUrl: './delete-client.component.html',
  styleUrls: ['./delete-client.component.css']
})
export class DeleteClientComponent implements OnInit {
user;
  constructor() { 

    this.user={
    id: 7,
    firstName: "Oussema",
    lastName: "GH1",
    adress: "Ariana essoughra",
    username: "FG1_client",
    email: "client1@esprit.tn",
    tel1: "+2165848672",
    tel2: "+2165454825",
    deliveryAdress: "Ariana essoughra, Nour JAAFAR",
    role: "client",
    image_id: 19,
    created_at: "2017-12-01 21:25:36",
    updated_at: "2017-12-01 21:25:36",
    image: {
            data: {
                id: 19,
                imgLink: "assets/img/users/atomix_user31.png",
                objectType: "user",
              created_at: "2017-11-28 15:40:19",
                created_at_human: "5 days ago"
                  }
            }
    }
  }

  ngOnInit() {
  }

}
