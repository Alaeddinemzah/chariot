import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintCmdComponent } from './print-cmd.component';

describe('PrintCmdComponent', () => {
  let component: PrintCmdComponent;
  let fixture: ComponentFixture<PrintCmdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintCmdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintCmdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
