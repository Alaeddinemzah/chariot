import{Image} from './Image';
import{Categorie} from './Categorie';
export class Event {

  id: number;

  status: string;
  category_id:number;
  categorie:Categorie;
  image:Image;
  images:Image[];
  created_at: string;
  created_at_human: string;


}