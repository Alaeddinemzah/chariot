import{Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import{Deal} from '../Deal';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class ProductsService{

  constructor(private http : Http)
  {
      console.log('ProductService Initialised...')
  }

  getListProducts()
  {  
    return this.http.get('../assets/products.json').map(res=>res.json());
  } 
  getRecommandedProducts()
  {  
    return this.http.get('../assets/recommanded_products.json').map(res=>res.json());
  }   
   
}