import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';

@Injectable()
export class ApiService {
  apiServer = 'http://localhost:8000';
  headers;


  constructor(private http: Http) {
    this.setHeaders();
  }

  setHeaders() {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  login(email, password) {
    let link: string = this.apiServer + '/oauth/token';
    let body = {
      'grant_type': 'password',
      'client_id': '2',
      'client_secret': 'H6PCjAgz2T5i9JOkOmXvLgxAzsuB7XbmjHE2UTBu',
      'username': email,
      'password': password,
      'scope': '*'
    };
    return this.http.post(link, body, {headers: this.headers})
      .map(res => res.json());
  }

  getAuthUser() {
    let link: string = this.apiServer + '/api/login';
    return this.http.get(link, {headers: this.headers})
      .map(res => res.json());
  }

  getDeals() {
    const link: string = this.apiServer + '/api/deals';
    return this.http.get(link, {headers: this.headers})
      .map(res => res.json());
  }
}
