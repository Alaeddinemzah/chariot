import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {CookieService} from 'angular2-cookie/core';

@Injectable()
export class AppService {
  user;

  constructor(private apiService: ApiService, private _cookieService: CookieService) {
  }


  // ****************** LOGIN
  login(login, password) {
    this.apiService.login(login, password)
      .subscribe(
        data => this.loginProcess(data),
        err => console.error(err)
      );

  }

  private loginProcess(data) {
    let token = 'Bearer ' + data.access_token;
    this.apiService.setHeaders();
    this.apiService.headers.append('Authorization', token);
    // time cookies  expired
    let time = Date.now() + (3600 * 1000);
    let expire = new Date(time);
    // get user info
    this.apiService.getAuthUser()
      .subscribe(
        data => {
          this.setDataCookies('user', data, {expires: expire});
          this.getCurrentLoggedUser();
        },
        err => console.error(err)
      );
  }

  logOut() {
    this.removeDataCookies('user');
    this.isLogged();
  }

  /**
   * Save user in user object from cookies
   */
  getCurrentLoggedUser() {
    if (this.isLogged()) {
      this.user = JSON.parse(this.getDataCookies('user'));
    }
  }

  /**
   * Verify if the user is logged or not
   * @return {boolean}
   */
  isLogged() {
    if (this.getDataCookies('user') !== undefined) {
      return true;
    }
    return false;
  }


  // ***********  COOKIES
  setDataCookies(key, data, option = {}) {
    this._cookieService.putObject(key, data, option);
  }

  getDataCookies(key): any {
    return this._cookieService.get(key);
  }

  removeDataCookies(key): any {
    return this._cookieService.remove(key);
  }


}
