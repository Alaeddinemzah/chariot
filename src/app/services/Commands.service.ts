import{Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class CommandsService{
  
  constructor(private http : Http)
  {
      console.log('CommandService Initialised...')
  }

  getListCommands()
  {    
      return this.http.get('../assets/commands.json').map(res=>res.json());
  }    
  
}