import{Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import{Deal} from '../Deal';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class EventsService{
  
  constructor(private http : Http)
  {
      console.log('EventService Initialised...')
  }

  getListEvents()
  {    
      return this.http.get('../assets/events.json').map(res=>res.json());
  }    
  
}