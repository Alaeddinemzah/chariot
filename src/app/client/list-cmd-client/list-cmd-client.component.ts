import { Component, OnInit } from '@angular/core';
import {Command} from '../../Command';
import {CommandsService} from '../../services/Commands.service';
@Component({
  selector: 'app-list-cmd-client',
  templateUrl: './list-cmd-client.component.html',
  styleUrls: ['./list-cmd-client.component.css'],
    providers :[CommandsService]
})
export class ListCmdClientComponent implements OnInit {


  ngOnInit() {
  }
 list_commands:Command[];
  constructor(private command_service:CommandsService) {
    this.command_service.getListCommands().subscribe(data => {this.list_commands = data});
    console.log(this.list_commands);
  }
}
