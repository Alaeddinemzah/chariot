import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientCommandComponent } from './client-command.component';

describe('ClientCommandComponent', () => {
  let component: ClientCommandComponent;
  let fixture: ComponentFixture<ClientCommandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientCommandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientCommandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
