import { Component, OnInit } from '@angular/core';
import {User} from '../../User';
import {UsersService} from '../../services/Users.service';
@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css'],
  providers :[UsersService],
})
export class ProfilComponent implements OnInit {
ngOnInit() {
  }
 list_users:User[];
 data;
 id_connected_user:number;
 user;
 //data :DataJson;
 //data :DataJson;

   constructor(private user_service:UsersService) {
     this.data={};
   this.user_service.getListUsers().subscribe(meta => {this.data = meta});
  // console.log(this.data.pagination.total);
   this.id_connected_user=3;

this.user={
    id: 7,
    firstName: "Oussema",
    lastName: "GH1",
    adress: "Ariana essoughra",
    username: "FG1_client",
    email: "client1@esprit.tn",
    tel1: "+2165848672",
    tel2: "+2165454825",
    deliveryAdress: "Ariana essoughra, Nour JAAFAR",
    role: "client",
    image_id: 19,
    created_at: "2017-12-01 21:25:36",
    updated_at: "2017-12-01 21:25:36",
    image: {
            data: {
                id: 19,
                imgLink: "assets/img/users/atomix_user31.png",
                objectType: "user",
              created_at: "2017-11-28 15:40:19",
                created_at_human: "5 days ago"
                  }
            }
    }


  //this.user_service.getUserById().subscribe(data => {this.user = data});
  //.subscribe(resp=>console.log(resp))
  //console.log(this.user);
  }
  
  getUserById(iduser:number) 
  {
    for(let result of this.list_users){

        if(result.id==iduser){
          return result;
        }
    }
  }

}
