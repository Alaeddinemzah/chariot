import { Component, OnInit } from '@angular/core';
import { DealsService } from '../services/Deals.service';
import {Deal} from '../Deal';

@Component({

    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    providers :[DealsService]

  })
export class HomeComponent {

  _deals:Deal[];

  constructor(private deals:DealsService){
    this._deals=[];
    this.deals.getRecommandedDeals().subscribe(data => {this._deals = data});
  }

}