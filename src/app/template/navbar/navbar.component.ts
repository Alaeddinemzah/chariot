import { Component, OnInit } from '@angular/core';
import { Categorie } from '../../Categorie';
import { CategoriesService } from '../../services/Categories.service';
import { AppService } from '../../services/app.service';

@Component({

  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers :[CategoriesService]

})

export class NavbarComponent  {
  categorie = Categorie;
  selectedCategorie :Categorie;
  nbItem;
  _categories:Categorie[];
    constructor(private categories:CategoriesService,private appService: AppService){
        this.nbItem=0;
        this.panier = JSON.parse(localStorage.getItem("panier"));
        if(this.panier!=null)
        {
            this.nbItem=this.panier.length;
        }
        this._categories=[];
        this._categories= this.categories.getListCategories();
       //  this.categories.getListCategories().subscribe(data => {this._categories = data});
        this.appService.getCurrentLoggedUser();
    } 
panier;

    deleteProductFromPanier()
    {

    }
    getListcategories(){
     
        this._categories =this.categories.getListCategories();
    }


}
