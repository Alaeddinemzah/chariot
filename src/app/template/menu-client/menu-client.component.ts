import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-menu-client',
  templateUrl: './menu-client.component.html',
  styleUrls: ['./menu-client.component.css']
})
export class MenuClientComponent implements OnInit {
url_client_profil:string;
url_client_commandes:string;
url_client_panier:string;
url_client_coupon:string;
url:string;
  constructor(router: Router) { 
      this.url_client_profil=" ";
    this.url_client_commandes=" ";
    this.url_client_panier=" ";
    this.url_client_coupon=" ";

     router.events.subscribe((url:any) => console.log(url));
      this.url=router.url;
      if(this.url=="/client/profil")
      {
        this.url_client_profil=" active";
      }
      if(this.url=="/client/commandes")
      {
        this.url_client_commandes=" active";
      }
      if(this.url=="/client/panier")
      {
        this.url_client_panier=" active";
      }
      if(this.url=="/client/coupons")
      {
        this.url_client_coupon=" active";
      }
  }

  ngOnInit() {
  }

}
