import { Component, OnInit } from '@angular/core';
import { SlideService } from '../../services/Slide.service';
import {Slide} from '../../Slide';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css'],
  providers :[SlideService]
})
export class SlideComponent {



      slides;
      _slide:Slide[];
  
    constructor(private slide:SlideService,){
      this._slide=[];
      this._slide= this.slide.getListSlide();
      this.slides = [ { imagePath : 'assets/img/slides/slide1.jpg'},
       { imagePath : 'assets/img/slides/slide2.jpg'},
       { imagePath : 'assets/img/slides/slide3.jpg'},
     //   { imagePath : 'assets/img/slides/slide4.jpg'},
      //  { imagePath : 'assets/img/slides/slide5.jpg'},
                  ]; 
   // this.deals.getListDeals().subscribe(data => {this._deals = data});
    }

    getListdeals(){
     
        this._slide =this.slide.getListSlide();
    }

      
    

    addImageToSlide(image : string){
      this.slides.push({imagePath:'\''+image+'\''} );

    }
    deleteImageFromSlide(id:number){

    this.slides.splice(id,1);

    }
    
}
