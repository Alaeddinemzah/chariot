import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-menu-gestionnaire',
  templateUrl: './menu-gestionnaire.component.html',
  styleUrls: ['./menu-gestionnaire.component.css']
})
export class MenuGestionnaireComponent  {
url_gest_add_deal:string;
url_gest_commandes:string;
url:string;
  constructor(router: Router) {
    this.url_gest_add_deal=" ";
    this.url_gest_commandes=" ";
    router.events.subscribe((url:any) => console.log(url));
      this.url=router.url;
      if(this.url=="/gestionnaire/deals/add")
      {
        this.url_gest_add_deal=" active";
      }
      if(this.url=="/gestionnaire/commandes")
      {
        this.url_gest_commandes=" active";
      }
   }



}
