import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-menu-admin',
  templateUrl: './menu-admin.component.html',
  styleUrls: ['./menu-admin.component.css']
})
export class MenuAdminComponent  {

url_admin_deals:string;
url_admin_deal_add:string;
url_admin_clients:string;
url_admin_gest_add:string;
url_admin_commandes:string;
url:string;
constructor(router: Router) { 
    
      this.url_admin_deals=" ";
      this.url_admin_deal_add=" ";
      this.url_admin_clients=" ";
      this.url_admin_gest_add=" ";
      this.url_admin_commandes=" ";

      router.events.subscribe((url:any) => console.log(url));
      this.url=router.url;
      if(this.url=="/admin/deals")
      {
        this.url_admin_deals=" active";
      }
      if(this.url=="/admin/deal/add")
      {
        this.url_admin_deal_add=" active";
      }
      if(this.url=="/admin/clients")
      {
        this.url_admin_clients=" active";
      }
      if(this.url=="/admin/gestionnaire/add")
      {
        this.url_admin_gest_add=" active";
      }
      if(this.url=="/admin/commandes")
      {
        this.url_admin_commandes=" active";
      }

}
}
