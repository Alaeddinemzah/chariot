import{Image} from './Image';
import{Categorie} from './Categorie';
export class Deal {

  id: number;
  title: string;
  price: number;
  remise: number;
  nbPurshaseR:number;
  nbPurshase:number;
  releaseDate: string;
  expiredDate: string;
  status: string;
  category_id:number;
  categorie:Categorie;
  image:Image;
  images:Image[];
  created_at: string;
  created_at_human: string;


}