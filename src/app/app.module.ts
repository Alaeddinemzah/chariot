import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AppRoutingModule} from './app.routing';
import {FormsModule} from '@angular/forms';
import {ApiService} from './services/api.service';
import {CookieService} from 'angular2-cookie/core';
import {HttpModule} from '@angular/http';


import { AppComponent } from './app.component';
import { NavbarComponent } from './template/navbar/navbar.component';
import { FooterComponent } from './template/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { DealComponent } from './deal/deal.component';
import { ProductComponent } from './product/product.component';
import { EventComponent } from './event/event.component';
import { MenuAdminComponent } from './template/menu-admin/menu-admin.component';
//admin
import {AddDealAdminComponent} from './admin/add-deal-admin/add-deal-admin.component';
import {AddGestionnaireComponent} from './admin/add-gestionnaire/add-gestionnaire.component';
import {AddSlideComponent} from './admin/add-slide/add-slide.component';
import {DeleteClientComponent} from './admin/delete-client/delete-client.component';
import {ListClientsComponent} from './admin/list-clients/list-clients.component';
import {ListCmdAdminComponent} from './admin/list-cmd-admin/list-cmd-admin.component';
import {ListDealsComponent} from './admin/list-deals/list-deals.component';
import {PrintCmdComponent} from './admin/print-cmd/print-cmd.component';

//client
import {CouponComponent} from './client/coupon/coupon.component';
import {ListCmdClientComponent} from './client/list-cmd-client/list-cmd-client.component';
import {PanierComponent} from './client/panier/panier.component';
import {ProfilComponent} from './client/profil/profil.component';
import { ClientCommandComponent } from './client/client-command/client-command.component';

//gestionnaire
import {AddDealGestionnaireComponent} from './gestionnaire/add-deal-gestionnaire/add-deal-gestionnaire.component';
import {ListCmdGestionnaireComponent} from './gestionnaire/list-cmd-gestionnaire/list-cmd-gestionnaire.component';




import { DetaildealComponent } from './deal/detaildeal/detaildeal.component';
import { SlideComponent } from './template/slide/slide.component';
import { Angular2TokenService } from 'angular2-token';
import { GestionnaireComponent } from './gestionnaire/gestionnaire.component';
import { MenuGestionnaireComponent } from './template/menu-gestionnaire/menu-gestionnaire.component';

import { MenuClientComponent } from './template/menu-client/menu-client.component';
import { LoginFormComponent } from './user/login-form/login-form.component';
import { ContactComponent } from './contact/contact.component';
import { DetailEventComponent } from './event/detail-event/detail-event.component';
import { RegisterComponent } from './user/register/register.component';
import {AppService} from "./services/app.service";





@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,

    HomeComponent,

    DealComponent,
    ProductComponent,
    EventComponent,
    
    DetaildealComponent,
  
    SlideComponent,
   
    GestionnaireComponent,
    MenuGestionnaireComponent,
    MenuAdminComponent,
    MenuClientComponent,
    PanierComponent,
    ProfilComponent,
    CouponComponent,
    PrintCmdComponent,
    DeleteClientComponent,
    AddGestionnaireComponent,
    ListClientsComponent,
    ListDealsComponent,
    AddSlideComponent,
    AddDealAdminComponent,
    AddDealGestionnaireComponent,
    ListCmdAdminComponent,
    ListCmdClientComponent,
    ListCmdGestionnaireComponent,
    LoginFormComponent,
    ClientCommandComponent,
    ContactComponent,
    DetailEventComponent,
    RegisterComponent 
  ],
   imports: [

BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
   
  ],
  providers: [Angular2TokenService, ApiService, CookieService,AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
