import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

import { AppComponent } from './app.component';
import { NavbarComponent } from './template/navbar/navbar.component';
import { FooterComponent } from './template/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { DealComponent } from './deal/deal.component';
import { ProductComponent } from './product/product.component';
import { EventComponent } from './event/event.component';
import { MenuAdminComponent } from './template/menu-admin/menu-admin.component';
//admin
import {AddDealAdminComponent} from './admin/add-deal-admin/add-deal-admin.component';
import {AddGestionnaireComponent} from './admin/add-gestionnaire/add-gestionnaire.component';
import {AddSlideComponent} from './admin/add-slide/add-slide.component';
import {DeleteClientComponent} from './admin/delete-client/delete-client.component';
import {ListClientsComponent} from './admin/list-clients/list-clients.component';
import {ListCmdAdminComponent} from './admin/list-cmd-admin/list-cmd-admin.component';
import {ListDealsComponent} from './admin/list-deals/list-deals.component';
import {PrintCmdComponent} from './admin/print-cmd/print-cmd.component';

//client
import {CouponComponent} from './client/coupon/coupon.component';
import {ListCmdClientComponent} from './client/list-cmd-client/list-cmd-client.component';
import {PanierComponent} from './client/panier/panier.component';
import {ProfilComponent} from './client/profil/profil.component';
import { ClientCommandComponent } from './client/client-command/client-command.component';

//gestionnaire
import {AddDealGestionnaireComponent} from './gestionnaire/add-deal-gestionnaire/add-deal-gestionnaire.component';
import {ListCmdGestionnaireComponent} from './gestionnaire/list-cmd-gestionnaire/list-cmd-gestionnaire.component';




import { DetaildealComponent } from './deal/detaildeal/detaildeal.component';
import { SlideComponent } from './template/slide/slide.component';
import { Angular2TokenService } from 'angular2-token';
import { GestionnaireComponent } from './gestionnaire/gestionnaire.component';
import { MenuGestionnaireComponent } from './template/menu-gestionnaire/menu-gestionnaire.component';

import { MenuClientComponent } from './template/menu-client/menu-client.component';
import { LoginFormComponent } from './user/login-form/login-form.component';
import { ContactComponent } from './contact/contact.component';
import { DetailEventComponent } from './event/detail-event/detail-event.component';
import { RegisterComponent } from './user/register/register.component';




const routes: Routes = [

{ path: 'home', component: HomeComponent },
{ path: 'deals', component: DealComponent },
{ path: 'contact', component: ContactComponent },
{ path: 'products', component:ProductComponent },
{ path: 'events', component:EventComponent },
{ path: 'event/:id', component:DetailEventComponent },
{ path: 'register', component:RegisterComponent },

{ path: 'deal/:id', component:DetaildealComponent },
{ path: 'slide', component:SlideComponent },
//{ path: 'client', component: LoginComponent },
  { path: 'client/commandes', component:ListCmdClientComponent },
  { path: 'client/command/:id', component: ClientCommandComponent},
  { path: 'client/panier', component: PanierComponent },
  { path: 'client/profil', component:ProfilComponent },
  { path: 'client/coupons', component: CouponComponent },
//{ path: 'admin', component: LoginComponent },
  { path: 'admin/deals', component:ListDealsComponent },
  { path: 'admin/deal/:id/edit', component:AddDealAdminComponent },
  { path: 'admin/deal/add', component: AddDealAdminComponent },
  { path: 'admin/clients', component: ListClientsComponent},
  { path: 'admin/client/:id/delete', component: DeleteClientComponent},
  { path: 'admin/gestionnaire/add', component: AddGestionnaireComponent},
  { path: 'admin/commandes', component: ListCmdAdminComponent},
  { path: 'admin/commandes/:id/print', component: PrintCmdComponent},
//{ path: 'gestionnaire', component: LoginComponent },
  { path: 'gestionnaire/commandes', component:ListCmdGestionnaireComponent },
  { path: 'gestionnaire/deals/add', component: AddDealGestionnaireComponent }
   ,
  { path: '**',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];


/*
const routes: Routes = [
  // {path: '', redirectTo: '/signin', pathMatch: 'full'},
  {
    path: '', component: IndexComponent, children:
    [
      {path: '', component: HomePageComponent},
      {path: 'basket', component: BasketComponent},
      {path: 'venteFlash', component: FlashSalePageComponent},
      {
        path: 'signUp', children:
        [
          {path: '', component: SignUpFormComponent},
          {path: 'client', component: ClientSignUpFormComponent},
          {path: 'seller', component: SellerSignUpFormComponent},
        ]
      },
      {path: 'dashboard', component: DashboardComponent}
    ]
  }
  // {
  //     path: 'panel', component: PanelComponent, children:
  //     [
  //         {path: 'dashboard', component: DashboardComponent},
  //         {path: 'project/add', component: ProjectformComponent},
  //         {path: 'project/edit/:id', component: ProjectformComponent},
  //         {path: 'project/details/:id', component: ProjectDetailsComponent},
  //         {path: 'search', component: ProjectListComponent}
  //     ]
  // },
  // {path: 'signin', component: AuthComponent},
  // {path: '', redirectTo: '/signin', pathMatch: 'full'}
];
*/
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {useHash: true, preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
